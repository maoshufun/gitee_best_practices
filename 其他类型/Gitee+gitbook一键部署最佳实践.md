### **简要**

看到小编题目《Gitee+gitbook一键部署最佳实践》征文，我想，gitbook不是一个文档平台么？原谅我仅仅在gitbok上看过几篇文档。。

![image-20190619162948386](http://ptfvncylo.bkt.clouddn.com/image-20190619162948386.png)

然后我就~~百度~~谷歌了一下。**弃用警告**。

## ⚠️ Deprecation warning:

As the efforts of the GitBook team are focused on the [GitBook.com](https://www.gitbook.com/) platform, the CLI is no longer under active development.
All content supported by the CLI are mostly supported by our [GitBook.com / GitHub integration](https://docs.gitbook.com/integrations/github).
Content hosted on the [legacy.gitbook.com](https://legacy.gitbook.com/) will continue working until further notice. For differences with the new vesion, check out our [documentation](https://docs.gitbook.com/v2-changes/important-differences).

简要来说，就是我们要做平台，做付费，不再积极开发了。

言归正传，gitbook 是一个命令行工具(和一个Node.js 库),通过git和markdown来构建书籍。

> GitBook is a command line tool (and Node.js library) for building beautiful books using GitHub/Git and Markdown (or AsciiDoc). 

在本文中，我们将结合gitee，完成一本《脱发指南》编写。

![image-20190619173146836](http://ptfvncylo.bkt.clouddn.com/image-20190619173146836.png)

整体流程如下

1. 本地编写书籍，提交gitee
2. 触发webhook，发送请求到服务器
3. 服务器接受请求，下拉代码，编译发布
4. 打开网页，获取最新的书籍

环境配置

1. CentOS 7.2 64位 服务器一台
2. 本地电脑一台
3. Gitee 账号

### **gitbook**

##### 环境要求

1. NodeJS (v4.0.0 and above is recommended)
2. Windows, Linux, Unix, or Mac OS X

使用npm安装gitbook-cli,如果没有node.js环境，请先安装node.js

```bash
[root@VM_0_4_centos ~]# yum install nodejs
```

安装完成后，看下当前版本

![image-20190620112335635](http://ptfvncylo.bkt.clouddn.com/image-20190620112335635.png)

下面得安装gitbook-cli

```bash
[root@VM_0_4_centos ~]# npm install gitbook-cli -g
```

安装完成后，看下当前版本，以及帮助

![image-20190620113203093](http://ptfvncylo.bkt.clouddn.com/image-20190620113203093.png)

下面我们得初始化一本书，并跑起来

```bash
[root@VM_0_4_centos ~]# gitbook init ./firstBook
warn: no summary file in this book
info: create README.md
info: create SUMMARY.md
info: initialization is finished
[root@VM_0_4_centos ~]#
```

执行命令后，我们看到新建了一个firstBook的文件夹，进入文件夹看下

```bash
[root@VM_0_4_centos ~]# ls
anaconda-ks.cfg  firstBook
[root@VM_0_4_centos ~]# cd firstBook/
[root@VM_0_4_centos firstBook]# ls
README.md  SUMMARY.md
[root@VM_0_4_centos firstBook]#
```

一个readme.md和一个summary.md的文件，打开瞅一眼

![image-20190620113833174](http://ptfvncylo.bkt.clouddn.com/image-20190620113833174.png)

发现只有一个简介的超链接，指向了README.md。看不出啥，我们先将book服务跑起来，看下跑起来后的效果，怎么样。

```bash
[root@VM_0_4_centos firstBook]# gitbook serve
Live reload server started on port: 35729
Press CTRL+C to quit ...

info: 7 plugins are installed
info: loading plugin "livereload"... OK
info: loading plugin "highlight"... OK
info: loading plugin "search"... OK
info: loading plugin "lunr"... OK
info: loading plugin "sharing"... OK
info: loading plugin "fontsettings"... OK
info: loading plugin "theme-default"... OK
info: found 1 pages
info: found 0 asset files
info: >> generation finished with success in 0.6s !

Starting server ...
Serving book on http://localhost:4000
```

我们浏览器打开4000端口，就可以看到了效果。这里，我们可以指定端口，也可以后台运行。

![image-20190620114307021](http://ptfvncylo.bkt.clouddn.com/image-20190620114307021.png)

跑起来，效果大概就是这样子了，左边是目录，右边是书籍内容。我们先停下服务，编辑一下书籍。咦，当你停了服务，后，发现多出了一个*_book*的文件夹，

```bash
[root@VM_0_4_centos firstBook]# ls
_book  README.md  SUMMARY.md
[root@VM_0_4_centos firstBook]#
```

使用tree命令看下(如果没有，记得安装一下),看到下图，大家估计也就猜到了，使用gitbook将我们写的md 文件，生成了一个html文件，html的内容，就是我们网页打开显示的内容

![image-20190620115023700](http://ptfvncylo.bkt.clouddn.com/image-20190620115023700.png)

好，我们继续编辑,新增一些文件，图片，下面都是一些简单的markdown语法，主要是链接引用。

![image-20190620124751541](http://ptfvncylo.bkt.clouddn.com/image-20190620124751541.png)

再次跑起来看下

![image-20190620131500172](http://ptfvncylo.bkt.clouddn.com/image-20190620131500172.png)

#### 小结

以上我们就基本完成了一本书籍的初期构建，以及发布。因为~~我也不是很熟悉gitbook~~本文的重点是结合gitee做到gitbook的一键部署发布，关于gitbook 以及gitbook 客户端工具还有很多内容没讲。关于gitbook相关的内容，可以参考下面的链接，**相信你构建的比我更加漂亮**

1. [Gitbook官方网址](https://www.gitbook.com/)
2.  [Gitbook项目地址](https://github.com/GitbookIO/gitbook)
3. [一些优秀的Gitbook文档](https://www.baidu.com/s?wd=gitbook入门)

### **webhook server**

#### 简介（copy from gitee help document）

码云 WebHook 功能是帮助用户 push 代码后，自动回调一个您设定的 http 地址。

> 这是一个通用的解决方案，用户可以自己根据不同的需求，来编写自己的脚本程序(比如发邮件，自动部署等)。

我们为了方便用户在第三方平台接收到码云的推送，已通过 WebHook 支持以下第三方应用

- 钉钉机器人: 设置 WebHook URL 为钉钉机器人地址。 具体请查看 [码云的 WebHook 增加对钉钉的支持](https://gitee.com/help/articles/4135)
- Slack 机器人: 设置 WebHook URL 为 Slack 机器人地址。

用户可以通过 **「仓库主页」->「管理页面」->「WebHooks」** 添加 WebHook

![输入图片说明](https://static.oschina.net/uploads/img/201806/06224100_WX2H.png)

目前, 码云支持以下 5 种钩子：

- Push: 仓库推送代码、推送 / 删除分支
- Tag Push: 新建 / 删除 tag
- Issue: 新建任务、变更任务状态、更改任务指派人
- Pull Request: 新建、更新pr代码、合并 Pull Request
- 评论: 评论仓库、任务、Pull Request、Commit

#### **搭建一个webhook服务**

随手在gitee上搜一下，https://gitee.com/hustcc/webhookit 有个现成的轮子，我们只要按照说明部署一下，就可以得到一个webhook server。

1. 安装(如果没有pip,先安装下pip),安装后，我们会得到两个命令webhookit_config和webhookit

   > **pip install webhookit**

2. 初始化配置(webhookit_config命令可以帮助我们生成一个默认的配置模板，我们根据我们自己的要求，修改其中的配置参数即可)

   > ```bash
   > mkdir webhookit_config/  #先建立一个目录
   > webhookit_config > ./webhookit_config/config.py #生成一个配置文件
   > ```

3. 修改配置文件

   > vi ./webhookit_config/config.py

   ![image-20190620183021726](http://ptfvncylo.bkt.clouddn.com/image-20190620183021726.png)

   >Python 变量名 `WEBHOOKIT_CONFIGURE` 不要去修改。

   > 每个 webhook 都用仓库的名字和分支名字 `'repo_name/branch_name'` 作为它的键值，每个 webhook 可以触发一组服务器，这些服务器的配置信息存储在一个数组中。

   > 服务器可以是远程的服务器，也可以是本地机器，如果要触发本机的脚本运行，那么请保持 `HOST`, `PORT`, `USER`, `PWD` 这些配置为空，或者不存在这些键值。

4. 编写cofig.sh 脚本

   > vi ./webhookit_config/config.sh
   >
   > ![image-20190620183542671](http://ptfvncylo.bkt.clouddn.com/image-20190620183542671.png)

5. 启动webhook serve,打开浏览器，18340端口

   > ```bash
   > [root@VM_0_4_centos ~]# webhookit -c ./webhookit_config/config.py
   > ```

   ![image-20190620190053479](http://ptfvncylo.bkt.clouddn.com/image-20190620190053479.png)

   其中我们可以看到我们webhook 服务配置，webhook url 就是我们需要copy 到项目webhook 配置的地址。其中注意一点，configure 里面，要配置项目名称和分支。

6. 测试一下。将webhook url copy到自己随便一个gitee项目中

   ![image-20190620190718596](http://ptfvncylo.bkt.clouddn.com/image-20190620190718596.png)

    点下测试，返现请求结果显示成功。![image-20190620190745871](http://ptfvncylo.bkt.clouddn.com/image-20190620190745871.png)

   看下服务器日志。hello world 打印成功。做到这一步，基本步骤已经ok了。我们在下面，将完成书籍内容的上传，以及脚本的完全编写

   ![image-20190620190908045](http://ptfvncylo.bkt.clouddn.com/image-20190620190908045.png)

#### 小结

​	以上，我们完成了一个webhook server 的搭建，并且通过简单的脚本完成了webhook 连通性的测试。其中webhookit 是我在gitee上随手找的一个轮子，大家也可以使用其他轮子，甚至自己写一个轮子(一个运行本地脚本命令功的http服务)。可能操作比较简单，在生产环境中，大家更加倾向与使用Jenkins  webhook 插件，来完成项目的集成部署，而开源中国本身，也有[Gitee Jenkins Plugin](https://gitee.com/oschina/Gitee-Jenkins-Plugin)插件，大家如果有兴趣，可以自己尝试一下,原理大致相同。关于webhookit文档，大家可以访问 [webhookit](https://gitee.com/hustcc/webhookit)

### 整合

#### 上传书籍到gitee上

1. gitee上新建一个项目

   ![image-20190620194450593](http://ptfvncylo.bkt.clouddn.com/image-20190620194450593.png)

2. 将本地书籍上传到仓库 

   ```bash
   [root@VM_0_4_centos ~]# cd firstBook/
   [root@VM_0_4_centos firstBook]# git init
   初始化空的 Git 版本库于 /root/firstBook/.git/
   [root@VM_0_4_centos firstBook]# ls
   author.md  _book  part1  part2  perface.md  README.md  SUMMARY.md
   [root@VM_0_4_centos firstBook]# ls -a
   .  ..  author.md  _book  .git  part1  part2  perface.md  README.md  SUMMARY.md
   [root@VM_0_4_centos firstBook]# git remote add origin https://gitee.com/littleHat/firstBook.git
   [root@VM_0_4_centos firstBook]# git pull origin master
   remote: Enumerating objects: 6, done.
   remote: Counting objects: 100% (6/6), done.
   remote: Compressing objects: 100% (6/6), done.
   remote: Total 6 (delta 0), reused 0 (delta 0)
   Unpacking objects: 100% (6/6), done.
   来自 https://gitee.com/littleHat/firstBook
    * branch            master     -> FETCH_HEAD
   error: Untracked working tree file 'README.md' would be overwritten by merge.
   [root@VM_0_4_centos firstBook]# git status
   # 位于分支 master
   # 要提交的变更：
   #   （使用 "git reset HEAD <file>..." 撤出暂存区）
   #
   #	删除：      .gitignore
   #	删除：      LICENSE
   #	删除：      README.en.md
   #	删除：      README.md
   #
   # 未跟踪的文件:
   #   （使用 "git add <file>..." 以包含要提交的内容）
   #
   #	README.md
   #	SUMMARY.md
   #	_book/
   #	author.md
   #	part1/
   #	part2/
   #	perface.md
   [root@VM_0_4_centos firstBook]# git add .
   [root@VM_0_4_centos firstBook]# git commit -m "脱发指南"
   [root@VM_0_4_centos firstBook]# git push origin master
   Username for 'https://gitee.com': nextroomman@gmail.com
   Password for 'https://nextroomman@gmail.com@gitee.com':
   Counting objects: 57, done.
   Compressing objects: 100% (50/50), done.
   Writing objects: 100% (55/55), 631.23 KiB | 0 bytes/s, done.
   Total 55 (delta 9), reused 0 (delta 0)
   remote: Powered By Gitee.com
   To https://gitee.com/littleHat/firstBook.git
      991ea19..eebf74e  master -> master
   ```

   ![image-20190621110707163](http://ptfvncylo.bkt.clouddn.com/image-20190621110707163.png)

   以上，我们便完成了代码的上传。具体的操作，可以查看gitee 的[帮助文档](https://gitee.com/help/articles/4122)。当然，如果你机器上，没有git环境，请先安装git环境，最好顺手把git config email /username 设置下，然后再生成一个ssh 公钥，并上传到gitee上。这样，我们下面编写脚本的时候，可以直接通过ssh 命令拉取代码。具体操作，请看这里：[操作手册](https://www.baidu.com)

3. 添加.ignore文件

   因为_book目录下的内容我们无需提交，我们只需要添加.ignore文件，并在里面加上

   ```
   _book/
   ```

   

#### 完成部署脚本的编写

作为新时代的程序员，DevOpser。不会写脚本哪行。我就不会。~~百度一下~~谷歌一下，好了。

```bash
#!/bin/bash
echo "Hello World !"
cd /root/firstBook  #写绝对路径比较好
ps -ef | grep "gitbook" | grep -v grep | awk '{print $2}' |xargs kill -9 #kill掉正在进行的服务
sleep 5s #等待kill完成
echo "start pull code"
git pull origin master #拉取代码
echo "start gitbook serve"
gitbook serve > nohub.out  2>&1  &  #后台启动服务
echo "Hello world end"
```

经过不断的百度，脚本config.sh 的脚本ok了。因为是第一次尝试动手写shell 脚本，不是很好，请多指教。

#### 测试

脚本准备好了，gitee仓库也建立好了。让我们来跑起来吧

1. 运行webhook server

   > webhookit -c ../webhookit_config/config.py  > webhookit.log 2>&1 & 

2. 配置webhook 地址

   ![image-20190621150959642](http://ptfvncylo.bkt.clouddn.com/image-20190621150959642.png)

3. 手动将gitbook 启动起来

   > gitbook serve >nohub.out 2>&1 &

   ![image-20190621151155963](http://ptfvncylo.bkt.clouddn.com/image-20190621151155963.png)

4. 本地电脑，下载代码，修改书籍，将脱发指南修改为程序员脱发指南。在本地机器，我们可以用更加灵活的markdown 编辑器，这里，我推荐使用typora。

   ```bash
   king:code king$ git clone git@gitee.com:littleHat/firstBook.git
   正克隆到 'firstBook'...
   remote: Enumerating objects: 66, done.
   remote: Counting objects: 100% (66/66), done.
   remote: Compressing objects: 100% (60/60), done.
   remote: Total 66 (delta 10), reused 0 (delta 0)
   接收对象中: 100% (66/66), 636.15 KiB | 537.00 KiB/s, 完成.
   处理 delta 中: 100% (10/10), 完成.
   ```

   打开typora编辑修改

   ![image-20190621151659057](http://ptfvncylo.bkt.clouddn.com/image-20190621151659057.png)

   提交代码

   ```bash
   king:firstBook king$ git add README.md
   king:firstBook king$ git commit -a "修改书籍名称"
   fatal: 路径和 -a 选项同时使用没有意义。
   king:firstBook king$ git commit -m "修改书籍名称"
   [master b512086] 修改书籍名称
    1 file changed, 1 insertion(+), 1 deletion(-)
   king:firstBook king$ git push
   枚举对象: 5, 完成.
   对象计数中: 100% (5/5), 完成.
   使用 8 个线程进行压缩
   压缩对象中: 100% (3/3), 完成.
   写入对象中: 100% (3/3), 382 bytes | 382.00 KiB/s, 完成.
   总共 3 （差异 1），复用 0 （差异 0）
   remote: Powered By Gitee.com
   To gitee.com:littleHat/firstBook.git
      c58d5b6..b512086  master -> master
   ```

   打开浏览器(这里有个插曲，我第一次打开浏览器的时候，发现并没有部署。。然后我就看gitee 的webhook的日志，提示{"data": "Not match the repo and branch or the webhook servers is not exist: firstBook/master", "success": false}，这里是因为webhook config.py 文件里面配置的不对，这一点大家一定要注意。)已经变成了程序员脱发指南

   ![image-20190621152631403](http://ptfvncylo.bkt.clouddn.com/image-20190621152631403.png)

#### 小结

 shell看起来简单，但是完全不熟悉，从0到1，还是各种坑。细节也是很重要。细节很重要，细节很重要。

### 总结

终于一步一步完成了gitee+gitbook的一键部署，因为是第一次使用，所以本篇文章，应该比较适合第一次搭建的选手。一套流程下来，给我最大的感受就是，实践出真知。很多原理大家都懂，但是一步一步操作，真的各种问题。